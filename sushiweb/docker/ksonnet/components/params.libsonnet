{
  global: {
    // User-defined global parameters; accessible to all component and environments, Ex:
    // replicas: 4,
  },
  components: {
    // Component-level parameters, defined initially from 'ks prototype use ...'
    // Each object below should correspond to a component in the components/ directory
    sushiweb: {
      containerPort: 8080,
      image: "schabrolles/sushiweb",
      name: "sushiweb",
      replicas: 1,
      servicePort: 8080,
      type: "ClusterIP",
      host: "sushi.10.7.11.23.nip.io",
    },
    namespace: {
      namespace: "sushiweb",
    },
  },
}
