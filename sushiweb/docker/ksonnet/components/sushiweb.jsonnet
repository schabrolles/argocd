local env = std.extVar("__ksonnet/environments");
local params = std.extVar("__ksonnet/params").components.sushiweb;
[{
    "apiVersion": "v1",
    "kind": "Service",
    "metadata": {
      "name": params.name
    },
    "spec": {
      "ports": [{
        "port": params.servicePort,
        "targetPort": params.containerPort,
        "name": "http"
      }],
      "selector": {
        "app": params.name
      },
      "type": params.type
    }
  },
  {
    "apiVersion": "apps/v1beta2",
    "kind": "Deployment",
    "metadata": {
      "name": params.name
    },
    "spec": {
      "replicas": params.replicas,
      "selector": {
        "matchLabels": {
          "app": params.name
        },
      },
      "template": {
        "metadata": {
          "labels": {
            "app": params.name
          }
        },
        "spec": {
          "containers": [{
            "image": params.image,
            "name": params.name,
            "ports": [{
              "containerPort": params.containerPort
            }]
          }]
        }
      }
    }
  },
  {
    "apiVersion": "route.openshift.io/v1",
    "kind": "Route",
    "metadata": {
      "labels": {
        "app.kubernetes.io/deploy-manager": "ksonnet",
        "ksonnet.io/component": params.name
      },
      "name": params.name
    },
    "spec": {
      "host": params.host,
      "port": {
        "targetPort": "http"
      },
      "to": {
        "kind": "Service",
        "name": params.name,
        "weight": 100
      },
      "wildcardPolicy": "None"
    },
    "status": {
      "ingress": [{
        "conditions": [{
          "status": "True",
          "type": "Admitted"
        }],
        "host": params.host,
        "routerName": "router",
        "wildcardPolicy": "None"
      }]
    }
  }
]