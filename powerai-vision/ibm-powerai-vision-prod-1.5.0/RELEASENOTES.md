# Release Notes

## What's new

### New to the 1.1.1 release:
- **Data augmentation** <br>
  After deploying a model, you can improve the model by using data augmentation to add modified images to the data set, then retraining the model. Data augmentation is the use of filters, such as blur and rotate, to create new versions of existing images. When you use data augmentation, a new data set is created that contains all of the existing images, plus the newly generated images. To learn more, see [Augmenting the data set](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.1/base/vision_augment.html?view=kc#vision_augment).
- **Optimize training for speed** <br>
  When training a data set, you can optimize the training for accuracy (default), or for speed. When training for speed, PowerAI Vision uses you only look once (YOLO) V2.
- **User account creation and management** <br>
  Use the powerai_vision_users.sh or powerai-vision-usermgt commands to work with users. For details, see [Managing users](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.1/base/vision_users.html?view=kc#vision_users).
- **User data isolation** <br>
  Users can only see and act on objects (data sets, files, trained models, and deployed models) that he or she owns. Objects are owned by the user who created them.
- **Improved notification management** <br>
  Sidebar with notifications on long-running background tasks.
 
### New to the 1.1.2 release:
- **Custom models** <br>
  You can now import custom TensorFlow models into PowerAI Vision and use them for training. For details, see [Working with custom models](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.2/base/vision_work_custom.html#vision_work_custom).
- **Advanced metrics** <br>
  New metrics are shown after a model is trained, providing more details on the performance of the model. For details, see [Understanding metrics](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.2/base/vision_metrics.html?view=kc).
- **Deployment to Enterprise FPGA accelerators (technology preview)** <br>
  Models trained using the YOLO model for object detection can be deployed to enterprise-class FPGA accelerators for inferencing. See [Deploying a trained model for more details](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.2/base/vision_deploy.html?view=kc#vision_deploy).
- **Multiple category classification results** <br>
  The results of image classification inferences now include multiple categories with the confidence levels for each category. See [Scenario: Classifying images](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.2/base/vision_scenario_categorizing.html?view=kc#vision_scenario_categorizing) for more information.
- **Support for new versions of IBM Cloud Private** <br>
  PowerAI Vision can now be installed with IBM Cloud Private 3.1 and later.
- **Non-administrator users** <br>
  You can now create users that are not administrators. These users can view and manage only resources that they created. See [Managing users](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.2/base/vision_users.html?view=kc#vision_users) for details.

### New to the 1.1.3 release:
- **GPU sharing for deployed models** <br>
  Deploying multiple models to a single GPU allows you to get the most out of your processing power. GPU sharing is supported only for GoogLeNet and Faster R-CNN models. For more information, see [Deploying a trained model](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.3/base/vision_deploy.html?view=kc#vision_deploy).
- **Train with a Detectron model** <br>
  You can now use a Detectron model to train a model. This allows you to train with objects that have been labeled as non-rectangular shapes. For details, see [Training a model](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.3/base/vision_train.html#vision_train).
- **Transfer learning** <br>
  You can use a model that was previously trained with PowerAI Vision as a base model to train new models. For details, see [Training a model](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.3/base/vision_train.html#vision_train).
- **Use non-rectangular shapes when labeling** <br>
  When labeling objects in a data set that will be used to train a Detectron model, you can use non-rectangular shapes. Non-rectangular labeling is supported in images, video frames, and with auto labeling. If you label objects with non-rectangular shapes and train the data set using a different model, associated rectangular bounding boxes are used. For more information, see [Labeling objects](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.3/base/vision_label.html?view=kc).
- **Support of COCO annotations** <br>
  Images with COCO annotations can be imported. Only object detection annotations are supported. For more information, see [Importing images with COCO annotations](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.3/base/vision_import_coco.html?view=kc#vision_import_coco).
- **Downloadable heat map** <br>
  You can download the heat map that is generated when testing an image with a deployed model.
- **Improved performance for inference** <br>
  Speeds when using the image classification (GoogLeNet) and object detection (Faster R-CNN) models for inference are improved. The improvement is especially significant for high-resolution images.
- **Improvements to the user interface** <br>
  The following changes have been made to the user interface to improve your experience:
  - **Heat map overlay:** When testing an image with a deployed model trained for classification, the heat map is layered on top of the image. You can then use a slider to set the opacity. This allows you to easily identify which areas of the image the algorithm is focusing on.
  - **Confidence threshold slider:** When testing an image with a deployed model, you can use the confidence slider to eliminate object labels that have low confidence.
  - **Trial information:** When using a trial version of PowerAI Vision, the remaining time is displayed in the header bar.
  - **GPU information:** You can view how many GPUs the system can access and how many of those are in use on the Models or Trained Models page.
  - **Improvements to labeling** <br>
    - The working image is given more screen space.
    - New Objects panel consolidates information about labeled objects and has new settings for labeling.
    - Labels on the image are shortened to two characters; with a corresponding list in the Objects panel.
    - New settings let you customize your labeling process. For example, you can change the outline color, hide previously drawn outlines, show or hide labels, and so on.
    - The list of labeled objects can be filtered.

### New to the 1.1.4 release:
- **Action identification** <br>
  You can train models to identify actions in a video. To get started, see Labeling actions (http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.4/base/vision_label_action.html?view=kc#vision_label_action).
- **Track usage metrics** <br>
  Administrators can track how many actions have been run in PowerAI Vision that require APIs. For details, see Monitoring usage metrics (http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.4/base/vision_usage_metrics.html?view=kc#vision_usage_metrics).
- **Inferencing for videos with Detectron models** <br>
  Detectron models now accept videos for inferencing; not just images.
- **Updates to the user interface** <br>
  The following changes have been made to the user interface to improve your experience:
  - The user name drop down menu now contains the following:
    - The log out action
    - Edition information (time trial and Non-production versions only)
    - Product version
  - A refresh button has been added to the GPU usage information.
  - The GPU usage drop-down now lists External GPUs. External GPUs are those that are used for processes outside of PowerAI Vision. For IBM Cloud Private installations, all GPUs are listed as External.
  - When auto capturing frames, you can now specify values as small as .001.
  - The Add new button has been renamed Add label on the Label Objects page.
  - Notifications will be sent that show active training progress.
  - Columns for Original file name, Actions, and Augment were added to the Data Set Details grid. The original file name will not be accurate, or will be missing for older data sets that are imported. Additionally, the original file name is not available for captured video frames or augmented files.

### New to the 1.1.5 release:
- **SSD model support** <br>
  Single Shot Detector (SSD) models are now available for training in PowerAI Vision. SSD models are suitable for real-time inference but are not as accurate as Faster R-CNN. For more information, see (Training a model)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_train.html?view=kc$vision_train].
- **GoogLeNet model Core ML support** <br>
  GoogLeNet models can be enabled for Core ML support. To enable Core ML support when training a GoogLeNet model, click Training options, then select Enable Core ML. You can download the Core ML assets from the Deployed Models page. For details, see (Training a model)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_train.html?view=kc$vision_train].
- **Use videos to test all object detection models** <br>
  You can now use videos to test all models trained for object detection. For information about testing models, see (Testing a model)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_train.html?view=kc$vision_train].
- **TensorRT support** <br>
  Single Shot Detector and Faster R-CNN models can now be enabled for TensorRT.  TensorRT enables deep learning on edge devices by maximizing inference performance, speeding up inferences, and delivering low latency across a variety of networks. When a model is enabled for TensorRT, downloadable TensorRT assets are generated. You can download the TensorRT assets from the Deployed models page. For details, see (Training a model)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_train.html?view=kc$vision_train].
- **Project groups** <br>
  Project groups allow a user to group data sets and trained models together.  PowerAI Vision lets you easily view and work with the assets associated with a project group. If you set the trained model status of your models, you can write scripts to automatically deploy the latest production ready model.  Additionally, you can automatically perform inferences on the most recently deployed model. For more information, see (Creating and working with project groups)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_project_groups.html?view=kc$vision_project_groups].
- **Production work flow** <br>
  The production work flow allows you to specify the status of each deployed model as untested, production, or rejected. You can optionally use the production work flow with project groups to quickly and easily work with the most recent deployed model of each status. For example, you can perform inferences on the newest untested deployed model without knowing the model ID.  Please see (Production work flow for more information.)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_pwf.html]
- **Auto deploy** <br>
  If production work flow is turned on for a project group, you can also turn on auto deploy. When Auto deploy is turned on, PowerAI Vision automatically deploys the most recent model with Untested and Production status. For more information, see (Automatically deploying the newest model.)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_autodeploy.html?view=kc$vision_autodeploy]
- **Inference history is available for all video testing** <br>
  The inference results for video testing are saved for seven days unless you delete them. To access the results of previous inferences, click Results history in the Test Model section of the Deployed Models page. You can open or delete any of the saved results.
- **Python 3 support for custom models** <br>
  Custom models must conform to Python 3. Any trained custom models from releases prior to Version 1.1.5 will not work if the custom model only supports Python 2. For more information about custom models, see (Preparing a model that will be used to train data sets in PowerAI Vision)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_prepare_custom_train.html?view=kc$vision_prepare_custom_train] and (Preparing a model that will be deployed in PowerAI Vision)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_prepare_custom_deploy.html?view=kc$vision_prepare_custom_deploy].
- **Auto labeling improvements** <br>
  Auto labeling has been changed in several ways: you can set the confidence threshold for adding labels, images with auto labels are marked in data sets, and you can save or reject individual auto labels. Additionally, you can view and filter on the confidence value for automatically added labels. This makes it easier to quickly reject labels that are likely incorrect, or to quickly accept labels that are likely accurate. For information, see (Automatically labeling objects.)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_auto_label.html?view=kc$vision_auto_label]
- **Improved metrics support** <br>
  The Usage Metrics page now includes more detail. Inferences are broken down by model, then are further broken down to give you more detailed information about the processes being run. Information about each file has also been added. For more information, see Monitoring usage metrics. (http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_usage_metrics.html?view=kc$vision_usage_metrics)
* CISO code signing
  You can verify the downloaded install tar file by using the CISO code signing service. For details, see (Installing PowerAI Vision stand-alone)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_install_standalone.html?view=kc$vision_install_standalone], (Installing PowerAI Vision with IBM Cloud Private)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_install_cloud.html?view=kc$vision_install_cloud], or (Installing Inference Server)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_inference_server.html?view=kc$vision_inference_server__install].
* Exported models are not encrypted
  PowerAI Vision no longer encrypts exported models. See (Importing and exporting PowerAI Vision)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_import_export.html] information for details.
* Containers will run with a non-root user ID
  All containers will run with a non-root user ID, which impacts install and upgrade. See (Installing, upgrading, and uninstalling PowerAI Vision)[http://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/base/vision_install.html?view=kc$vision_install].
* User interface improvements
  The following changes have been made to the user interface:
  * Drag and drop videos when testing
    You can upload videos for testing either by navigating to the file or by drag and drop.
  * Updated look and feel of Training page
    The Training button and Advanced settings options were moved to the top toolbar. Additionally, the options are now on descriptive cards instead of radio buttons.
  * Preview contrast and brightness
    The settings panel in the Label Objects page lets you add a Preview contrast and Preview brightness filter on the image being edited. This is a temporary filter to make labeling easier and does not modify the original image in any way.


## Fixes

## Prerequisites

## Installing

You can install PowerAI Vision stand-alone or PowerAI Vision with IBM Cloud Private. For more information, see the [Installing PowerAI Vision topic](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/rn/main.htm).

## Limitations
The following are the limitations for IBM PowerAI Vision Version 1.1.5:
* If you import a .zip file into an existing data set, the .zip file cannot contain a directory structure.
* If models are imported that were trained with versions of the product prior to 1.1.2, they will not have model training details.
* If models were deployed prior to an upgrade to 1.1.5, the models must be redeployed.
* PowerAI Vision uses an entire GPU when you are training a dataset and when a REST API endpoint is deployed (even if the endpoint is idle). The number of active GPU tasks (model training and deployment) that you can run at the same time depends on the number of GPUs on your Power® Systems server. For example, if your Power Systems™ server has four GPUs, you can have a maximum of four training and inference containers running simultaneously. With the 1.1.3 release, multiple FRCNN and GoogLeNet models can be deployed to a single container, providing improved GPU utilization.  You must verify that there are enough available GPUs on the system for the desired workload.  The number of GPUs in the environment and their utilization is displayed in the UI to help the user determine if there are available resoures.

## Version History

## Breaking Changes

## Documentation
[IBM PowerAI Vision 1.1.5 Knowledege Center](https://www.ibm.com/support/knowledgecenter/SSRU69_1.1.5/navigation/welcome.html)

