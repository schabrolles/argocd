LICENSE INFORMATION

The Programs listed below are licensed under the following License Information terms and conditions in addition to the Program license terms previously agreed to by Client and IBM. If Client does not have previously agreed to license terms in effect for the Program, the International Program License Agreement (Z125-3301-14) applies.

Program Name (Program Number):
IBM Visual Insights 1.2 (5737-H10)
IBM Visual Insights 1.2 (5765-TAI)

The following standard terms apply to Licensee's use of the Program.

Limited use right

As described in the International Program License Agreement ("IPLA") and this License Information, IBM grants Licensee a limited right to use the Program. This right is limited to the level of Authorized Use, such as a Processor Value Unit ("PVU"), a Resource Value Unit ("RVU"), a Value Unit ("VU"), or other specified level of use, paid for by Licensee as evidenced in the Proof of Entitlement. Licensee's use may also be limited to a specified machine, or only as a Supporting Program, or subject to other restrictions. As Licensee has not paid for all of the economic value of the Program, no other use is permitted without the payment of additional fees. In addition, Licensee is not authorized to use the Program to provide commercial IT services to any third party, to provide commercial hosting or timesharing, or to sublicense, rent, or lease the Program unless expressly provided for in the applicable agreements under which Licensee obtains authorizations to use the Program. Additional rights may be available to Licensee subject to the payment of additional fees or under different or supplementary terms. IBM reserves the right to determine whether to make such additional rights available to Licensee.

Specifications

Program's specifications can be found in the collective Description and Technical Information sections of the Program's Announcement Letters.

Prohibited Uses

Licensee may not use or authorize others to use the Program if failure of the Program could lead to death, bodily injury, or property or environmental damage.

Separately Licensed Code

The provisions of this paragraph do not apply to the extent they are held to be invalid or unenforceable under the law that governs this license. Each of the components listed below is considered "Separately Licensed Code". IBM Separately Licensed Code is licensed to Licensee under the terms of the applicable third party license agreement(s) set forth in the NON_IBM_LICENSE file(s) that accompanies the Program. Notwithstanding any of the terms in the Agreement, or any other agreement Licensee may have with IBM, the terms of such third party license agreement(s) governs Licensee's use of all Separately Licensed Code unless otherwise noted below.

Future Program updates or fixes may contain additional Separately Licensed Code. Such additional Separately Licensed Code and related licenses are listed in another NON_IBM_LICENSE file that accompanies the Program update or fix. Licensee acknowledges that Licensee has read and agrees to the license agreements contained in the NON_IBM_LICENSE file(s). If Licensee does not agree to the terms of these third party license agreements, Licensee may not use the Separately Licensed Code.

For Programs acquired under the International Program License Agreement ("IPLA") or International Program License Agreement for Non Warranted Program ("ILAN") and Licensee is the original licensee of the Program, if Licensee does not agree with the third party license agreements, Licensee may return the Program in accordance with the terms of, and within the specified time frames stated in, the "Money-back Guarantee" section of the IPLA or ILAN IBM Agreement.

Note: Notwithstanding any of the terms in the third party license agreement, the Agreement, or any other agreement Licensee may have with IBM:
(a) IBM provides this Separately Licensed Code to Licensee WITHOUT WARRANTIES OF ANY KIND;
(b) IBM DISCLAIMS ANY AND ALL EXPRESS AND IMPLIED WARRANTIES AND CONDITIONS INCLUDING, BUT NOT LIMITED TO, THE WARRANTY OF TITLE, NON-INFRINGEMENT OR INTERFERENCE AND THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, WITH RESPECT TO THE SEPARATELY LICENSED CODE;
(c) IBM is not liable to Licensee, and will not defend, indemnify, or hold Licensee harmless for any claims arising from or related to the Separately Licensed Code; and
(d) IBM is not liable for any direct, indirect, incidental, special, exemplary, punitive or consequential damages including, but not limited to, lost data, lost savings, and lost profits, with respect to the Separately Licensed Code.

Notwithstanding these exclusions, in Germany and Austria, IBM's warranty and liability for the Separately Licensed Code is governed only by the respective terms applicable for Germany and Austria in IBM license agreements.

Note: IBM may provide limited support for some Separately Licensed Code. If such support is available, the details and any additional terms related to such support will be set forth in the License Information document.

The following are Separately Licensed Code:
IMAGE [leaflet]
EAFLET-SRC.ESM.JS [leaflet]
MSPIMAGEPLUGIN.PY [pillow]
_LAYOUTS [leaflet]
chapters.en.vtt
howto.diveintomark
JCIP-ANNOTATIONS
org.jgroups.annotations
GuardedBy.java
CONCURRENT [fabric]
/pbr-4.0.2-0/doc/source/user/semver.rst - semver.rst
crulp/ligatures
MODEL_ZOO [Detectron]
descriptions
very_deep weights
GO-DIGEST [kubernetes]
SPDYSTREAM [kubernetes]
CONTAINERD [kubernetes]
Angular
libdc1394-22 2.2.4-1
texinfo 6.1.0.dfsg.1-5
zvbi 0.2.35-10
freeimage 3.17.0
libfreeimage3 3.17.0+ds1-2
libzvbi-common 0.2.35-10
ua-parser-js 0.7.17
uWSGI 2.0.17
yaml-cpp 0.5.2-3
va-driver-all 1.7.0-1
libshine3 3.1.0-4
mercurial 3.7.3
python-pip-whl 8.1.1-2ubuntu0.4
python-pip 8.1.1-2ubuntu0.4
NVIDIA CUDA-GDB
aclocal.m4
apt 1.2.10
apt postrm
attributes.m4
ffmpeg
CSIV2IORToSocketInfo.java
debian-archive-keyring 2017.5
libfreeimage-dev
weld-spi
texi2pod.pl
fonts-lyx 2.1.4-2
narayana-jts-idlj
libjpeg62-turbo
jboss-concurrency-api_1.0_spec 1.0.0
jboss-el-api_3.0_spec 1.0.9
Jinja2
libutvideodec.cpp
libfreeimage-dev 3.17.0+ds1-2
openjdk-orb 8.0.8.Final
org.wildfly.iop.openjdk.naming
pkg-config 0.29.1
pkgconf
OpenMPI
ucf 14
doc/t2h.pm
libjpeg62-turbo
ltmain.sh
Natural Docs
python-daemon-2.0.5/setup.py
rcue.css
NOTO-FONTS [pillow]
FastInfoset 1.2.13
freebXML License v1.1
gnuplot-tex
gnuplot5-data
HDF5
hdfgifwr
HDF5 (Hierarchical Data Format 5) Software Library and Utilities
NCSA HDF5 (Hierarchical Data Format 5) Software Library and Utilities
Jasper
NewsMLv1.0.dtd
pythag
wxWidgets
Adobe Glyph List For New Fonts
aglfn.txt
FIG
glgrab.cpp
ImageMagick
src/motif/mdi
Swig
fontconfig
gnupg-curl 1.4.20-1ubuntu3.1  (doc/OpenPGP)
UBUNTU FONT LICENCE Version 1.0
ws-policy.xsd
MD5C.C - RSA
MD5 message-digest algorithm
org.jboss.modules.xml
oasis-200401-wss-wssecurity-secext-1.0.xsd
JSONArray.java

Third Party Operating Systems

For the convenience of Licensee, the Program may be accompanied by a third party operating system. The operating system is not part of the Program, and is licensed directly by the operating system provider (e.g., Red Hat Inc., Novell Inc., etc.) to Licensee. IBM is not a party to the license between Licensee and the third party operating system provider, and includes the third party operating system "AS IS", without representation or warranty, express or implied, including any implied warranty of merchantability, fitness for a particular purpose or non-infringement.

Export and Import Restrictions

This Program may contain cryptography. Transfer to, or use by, users of the Program may be prohibited or subject to export or import laws, regulations or policies, including those of the United States Export Administration Regulations. Licensee assumes all responsibility for complying with all applicable laws, regulations, and policies regarding the export, import, or use of this Program, including but not limited to, U.S. restrictions on exports or reexports. To obtain the export classification of this Program refer to: https://www.ibm.com/products/exporting/.

The following units of measure may apply to Licensee's use of the Program.

Virtual Server

Virtual Server is a unit of measure by which the Program can be licensed. A server is a physical computer that is comprised of processing units, memory, and input/output capabilities and that executes requested procedures, commands, or applications for one or more users or client devices. Where racks, blade enclosures, or other similar equipment is being employed, each separable physical device (for example, a blade or a rack-mounted device) that has the required components is considered itself a separate server. A virtual server is either a virtual computer created by partitioning the resources available to a physical server or an unpartitioned physical server. Licensee must obtain Virtual Server entitlements for each virtual server made available to the Program, regardless of the number of processor cores in the virtual server or the number of copies of the Program on the virtual server.

L/N:  L-CKIE-BL43TM
D/N:  LC27-8328-00
P/N:  L-CKIE-BL43TM


