# le-gendarme

Le-Gendarme is a simple containerized kubectl process which look after paiv inference to apply affinity policy based on node label.
It uses `kubectl patch` in order to apply new deployment policy and toleration policy on paiv-deployment with label `dltask-type=INFER`.
This allow to favor old/non performant GPU for Inference and keep "Gold" GPU during more demanding training task.    

kubernetes deployment patch is provided in YAML via configMap (patch-inference.yml)

## Installing the Chart

To install the chart with the release name `my-release`:

```bash
$ helm install --name my-release sushiweb
```

> **Tip**: List all releases using `helm list`

## Verifying the Chart
See NOTES.txt associated with this chart for verification instructions

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```bash
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.  If a delete can result in orphaned components include instructions additional commands required for clean-up.  

For example :

When deleting a release with stateful sets the associated persistent volume will need to be deleted.  
Do the following after deleting the chart release to clean up orphaned Persistent Volumes.

```console
$ kubectl delete pvc -l release=my-release
``` 

## Configuration
The following tables lists the configurable parameters of the IBM PostgreSQL chart and their default values.

| Parameter                            | Description                                     | Default                                                    |
| ----------------------------------   | ---------------------------------------------   | ---------------------------------------------------------- |
| `arch.ppc64le`                | `Ppc64le worker node scheduler preference in a hybrid cluster` | `2 - No preference` - worker node is chosen by scheduler       |
| `image`                              | `le-gendarme` image repository                   | `registry.gitlab.com/pslc/docker/le-gendarme`   |
| `imageTag`                           | `le-gendarme` image tag                          | `1.13`                                                    |
| `imagePullPolicy`                    | Image pull policy                               | `Always` if `imageTag` is `latest`, else `IfNotPresent`    |
| `conf.sleepTime`                          | Sleep time in second between each deployment check | `10`                                                     |
| `conf.patchInfer`                     | Patch Inference Deployment Activation (based on Configmap) | `false`                        |
| `conf.patchTrain`                     | Patch Training Deployment Activation (based on Configmap) | `false`                        |
| `conf.inferDeployDurationLimit`           | Kill Inference Deployment after an defined amount of time (in s) | `-1` => unlimited   |
| `conf.inferMaxPerUser`           | Limit the number of inference per user. (Kill the oldes inference) | `-1` => unlimited                             |
| `vision.url`           | url of AI vision UI (http://powerai-vision/powerai-vision) | `http://powerai-vision-vision/powerai-vision-vision` |
| `vision.adminUser`           | PowerAI vision admin username | `admin`                            |
| `vision.adminPassword`           | PowerAI vision admin password | n/a                             |
| `resources`                          | CPU/Memory resource requests/limits             | Memory: `100Mi`, CPU: `200m`                               |


Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

> **Tip**: You can use the default [values.yaml](values.yaml)

## Architecture

- This dirty hack is only available for Power for the moment
  - ppc64le
