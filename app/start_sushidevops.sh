#!/bin/bash

for yaml in sushi-proxy.yaml sushi-dev.yaml sushi-x86.yaml sushi-Z.yaml sushi-power.yaml; do
    argocd app create -f $yaml
done